module.exports = function(grunt) {

    // 1. All configuration goes here 
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

watch: {
        
        files: ['js/*.js'],
        files: ['wp-content/themes/powersportswraps/library/scss/**/*.scss' , 
        'wp-content/themes/powersportswraps/library/css/style.min.css'],
        tasks: ['concat', 'uglify', 'sass', 'cssmin'],
        options: {
        spawn: false,
                }
        },

concat: {   
    dist: {
        src: [
            'wp-content/themes/powersportswraps/library/js/libs/*.js', // All JS in the libs folder
            'wp-content/themes/powersportswraps/library/js/global.js',  // This specific file
            'wp-content/themes/powersportswraps/library/js/scripts.js'  // This specific file

        ],
        dest: 'wp-content/themes/powersportswraps/library/js/build/production.js',
    }
       },

 uglify: {
    build: {
        src: 'wp-content/themes/powersportswraps/library/js/build/production.js',
        dest: 'wp-content/themes/powersportswraps/library/js/build/production.min.js'
    }
},

sass: {
            compile: {
                files: {
                    'wp-content/themes/powersportswraps/library/css/style.css':
                     'wp-content/themes/powersportswraps/library/scss/**/style.scss',
                },
          
            },
        },

cssmin: {
        my_target: {
            src: 'wp-content/themes/powersportswraps/library/css/style.css',
            dest: 'wp-content/themes/powersportswraps/library/css/style.min.css'
        }
    }, 
        

      // cssmin: {
      //     minify: {
      //       cwd: 'wp-content/themes/powersportswraps/library/css/',
      //       src: ['wp-content/themes/powersportswraps/library/css/style.css'],
      //       dest: 'wp-content/themes/powersportswraps/library/css/',
      //       ext: '.min.css'
      //     }
      //   },  


browserSync: {
    dev: {
        bsFiles: {
     src : ['wp-content/themes/powersportswraps/library/css/style.min.css', 
     'wp-content/themes/powersportswraps/*.php']
        },
        options: {
                 watchTask: true,
            debugInfo: true,
            proxy: 'powersportwraps.dev',
            ghostMode: {
                scroll: false,
                links: false
                        }//options
                }
        }
    }

     

 
    });
    

 // 3. Where we tell Grunt we plan to use this plug-in.
    grunt.loadNpmTasks('grunt-browser-sync');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-watch');

    // 4. Where we tell Grunt what to do when we type "grunt" into the terminal.
    grunt.registerTask('default', ["browserSync", "watch"]);
	grunt.registerTask(['browserSync', 'uglify', 'sass', 'cssmin', 'concat' , 'watch' ]);


};